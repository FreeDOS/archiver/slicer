{$I-}
procedure TArchiveFile.ExtractArchive;
var
    F : File;
    P : Pointer;
    Chunk : TSAF_AnyChunk;
{$IFOPT D+} DebugSize : LongInt; {$ENDIF}
    X, O, S : longInt;
    BytesRead, ToRead : word;
    {LastDir,}
    LastText, LastFile, LastSig, LastSkip,
    LastStamp : longint;
    LastAttr, LastComp  : word;
    TempLang, TempName, TempDir, LastFileName : String;
    MsgPrefix : String[10];
    Buf : Pointer;
    ThisCat, NeedConf : boolean;
    IOE, ROE : integer;
begin
{$IFOPT D+} Debug('+extractarchive', Name); {$ENDIF}
    P := @Chunk;
    OpenArchive('SAF_OPEN', not Report);
    LastFile := -1;
    {LastDir  := -1;}
    LastText := -1;
    LastSig  := -1;
    LastSkip := -1;
    LastAttr := $FFFF;
    LastStamp := -1;
    LastComp := cpNone;
    LastFileName := '';
    TempLang := StringDelim;
    ThisCat := False;
    NeedConf := False;
    CheckMemory(BufferSize);
    GetMem(Buf, BufferSize);
    if Report then
        MsgPrefix := 'REP_'
    else begin
        MsgPrefix := 'EXT_';
        if not DirExists(Output) then begin
            with TSAF_Directory(P^) do
                MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix + 'DIR',
                    IntStr(-1) + FormatDelim + SizeStr(0) + FormatDelim +
                    Output + FormatDelim + IntStr(-1) + FormatDelim +
                    FormatDelim ));
            CreateDir(Output, 0, 0);
         end;
    end;

    repeat
       while not EOF do begin
            X := FilePos;
{$IFOPT D+} DebugSize := FileSize;  {$ENDIF}
            ReadRecord(Chunk.Block, Sizeof(Chunk.Block) );
{$IFOPT D+} Debug('+chunk', IntStr(Chunk.Block.ID) + SPACE_char +
IntStr(Chunk.Block.Size) + SPACE_char + IntStr(X) + '/' +IntStr(DebugSize)); {$ENDIF}
            case Chunk.Block.ID of
                scNull : begin
                    MaybeTextLn(mcGeneral, ParseMessage( MsgPrefix + 'NULL',
                    IntStr(Chunk.Block.ID) + FormatDelim + SizeStr(Chunk.Block.Size)));
                end;
                scPart : SpringCleaning;
                scCategory : begin
{$IFOPT D+} Debug('+category', IntStr(Sizeof(TSAF_Category))); {$ENDIF}
                    UserAccept(NeedConf);
                    TempLang := StringDelim;
                    if Chunk.Block.Size > Sizeof(TSAF_Category) then
                        MakeError(@Self, erInvalid_Data, True);
                    ReadRecord(Chunk.Data, Chunk.Block.Size - Sizeof(Chunk.Block) );
                    if Chunk.Data[Chunk.Block.Size - Sizeof(Chunk.Block) - 1] <> 0 then
                        MakeError(@Self, erInvalid_Data, True);

                    LastCategory := PascalStr(TSAF_Category(P^).Name);
                    {$IFOPT D+} Debug('', PascalStr(TSAF_Category(P^).Name)); {$ENDIF}
                    ThisCat := CompareCategories;
                    if ThisCat then
                        with TSAF_Category(P^) do
                            MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix + 'CAT',
                                IntStr(Block.ID) + FormatDelim + SizeStr(Block.Size) + FormatDelim +
                                LastCategory ));
{$IFOPT D+} Debug('-', ''); {$ENDIF}
                end;
                scDirectory : begin
{$IFOPT D+} Debug('+directory', ''); {$ENDIF}
                    UserAccept(NeedConf);
                    TempLang := StringDelim;
                    if ThisCat then begin
                        if Summary.DirTotal = 0 then Summary.DirTotal := 1;
                        if Chunk.Block.Size > Sizeof(TSAF_Directory) then
                            MakeError(@Self, erInvalid_Data, True);
                        ReadRecord(Chunk.Data, Chunk.Block.Size - Sizeof(Chunk.Block));
                        if Chunk.Data[Chunk.Block.Size - Sizeof(Chunk.Block) - 1] <> 0 then
                            MakeError(@Self, erInvalid_Data, True);
                        TempName := PascalStr(TSAF_Directory(P^).Name);
{$IFOPT D+} Debug('', '(' + IntStr(TSAF_Directory(P^).Uniq) + ') ' + TempName); {$ENDIF}
                        O := Dirs^.Find(TempName);
                        Dirs^.Put(TSAF_Directory(P^).Uniq, TempName);
                        if ((O = -1) or ((O <> TSAF_Directory(P^).Uniq) and Report
                        and (Verbose > 0))) and
                        (not NoEmptyDir) then begin
                            if Report or (not DirExists(Output + TempName))
                            or (Verbose > 0) then
                            with TSAF_Directory(P^) do begin
                                MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix + 'DIR',
                                    IntStr(Block.ID) + FormatDelim + SizeStr(Block.Size) + FormatDelim +
                                    WhichStr(Report, Output + TempName, TempName) +
                                    FormatDelim + IntStr(Uniq) + FormatDelim +
                                    AttribStr(Attrib) + FormatDelim + StampStr(Stamp) ));
                                if not Report then
                                    CreateDir(Output + TempName, Attrib, Stamp);
                            end;
                        end;
                        if O = -1 then begin
                            O := Dirs^.Find(TempName);
                            if O <> -1 then
                                Inc(Summary.DirTotal);
                        end;
                        {LastDir := TSAF_Directory(P^).Uniq;}
                    end;
{$IFOPT D+} Debug('-', ''); {$ENDIF}
                end;
                scFile : begin
{$IFOPT D+} Debug('+file', ''); {$ENDIF}
                    UserAccept(NeedConf);
                    TempLang := StringDelim;
                    if ThisCat then begin
                        if Summary.DirTotal = 0 then Summary.DirTotal := 1;
                        if Chunk.Block.Size <= Sizeof(TSAF_File) - Sizeof(TFileName) + 2 then
                            MakeError(@Self, erInvalid_Data, True);
                        BlockRead(Chunk.Data, Sizeof(TSAF_File) - Sizeof(Chunk.Block), BytesRead);
                        TempName := PascalStr(TSAF_File(P^).Name);
                        if TempName = '' then
                            MakeError(@Self, erInvalid_Data, True);
                        O := Sizeof(TSAF_File) - Sizeof(TFileName) + Length(TempName) + 1;
                        S := Chunk.Block.Size - O;

 {$IFOPT D+} Debug('',  IntStr(Sizeof(TSAF_File)) + SPACE_char +
                        IntStr(Sizeof(TFileName)) + SPACE_char +
                        IntStr(Length(TempName) + 1) + SPACE_char +
                        IntStr(O) + SPACE_char +
                        IntStr(S) + SPACE_char +
                        IntStr(TSAF_File(P^).Size) + SPACE_char
 ); {$ENDIF}
                       if TSAF_File(P^).Dir <> -1 then begin
                            TempDir := TailDelim(Dirs^.Get(TSAF_File(P^).Dir));
                            if TempDir = '' then
                                MakeError(@Self, erInvalid_Data, Not Report);
                        end else
                            TempDir := '';

                        if (Chunk.Data[O - Sizeof(Chunk.Block) - 1] <> 0) or
                        (S > TSAF_File(P^).Size) then
                            MakeError(@Self, erInvalid_Data, True);
                            {ShowError('', IntStr(erInvalid_Data) + FormatDelim  +
                            TempName, erInvalid_Data, True);}
{$IFOPT D+} Debug('', '[' + IntStr(TSAF_File(P^).Uniq) + '] ' +
TempName + ' (' + IntStr(TSAF_File(P^).Dir) + ')'); {$ENDIF}
                        with TSAF_File(P^) do begin
                            if (Offset = 0) and (Not Report) then begin
                                CreateDir(Output + TempDir, 0, 0);
                                LastSkip := -1;
                                if FileExists(Output + TempDir + TempName) then begin
                                    if Overwrite = 0 then begin
                                        if not AskUser('OVER', TempName + FormatDelim +
                                        TempDir ) then
                                            LastSkip := Uniq;
                                    end else if Overwrite < 0 then
                                        LastSkip := Uniq;

                                    if LastSkip = Uniq then
                                        ThemeColor(clSkipped)
                                    else
                                        ThemeColor(clOverwrite);
                                    MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix +
                                        WhichStr(LastSkip <> Uniq, 'SKIP', 'RPCL'),
                                        IntStr(Block.ID) + FormatDelim + SizeStr(Block.Size) + FormatDelim +
                                        TempName + FormatDelim + IntStr(Uniq) + FormatDelim +
                                        AttribStr(Attrib) + FormatDelim + StampStr(Stamp) +  FormatDelim +
                                        IntStr(Offset) + FormatDelim + FirstWord(SizeStr(S)) + FormatDelim +
                                        SizeStr(Size) + FormatDelim + TempDir + FormatDelim +
                                        IntStr(TSAF_File(P^).Dir) ));
                                    ThemeColor(clNormal);
                                end;
                            end;

                            if (LastSkip <> Uniq) then begin
                                MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix
                                    + WhichStr(S < Size, 'FILE', + 'PART'),
                                    IntStr(Block.ID) + FormatDelim + SizeStr(Block.Size) + FormatDelim +
                                    TempName + FormatDelim + IntStr(Uniq) + FormatDelim +
                                    AttribStr(Attrib) + FormatDelim + StampStr(Stamp) +  FormatDelim +
                                    IntStr(Offset) + FormatDelim + FirstWord(SizeStr(S)) + FormatDelim +
                                    SizeStr(Size) + FormatDelim + TempDir + FormatDelim +
                                    IntStr(TSAF_File(P^).Dir) ));
                                MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix
                                    + WhichStr(S < Size, 'FILE', + 'PART') + '2',
                                    IntStr(Block.ID) + FormatDelim + SizeStr(Block.Size) + FormatDelim +
                                    TempName + FormatDelim + IntStr(Uniq) + FormatDelim +
                                    AttribStr(Attrib) + FormatDelim + StampStr(Stamp) +  FormatDelim +
                                    IntStr(Offset) + FormatDelim + FirstWord(SizeStr(S)) + FormatDelim +
                                    SizeStr(Size) + FormatDelim + TempDir + FormatDelim +
                                    IntStr(TSAF_File(P^).Dir)));
                            end;

                            if LastFile <> Uniq then
                                LastSig := 0;

                            if (Not Report) and (LastSkip <> Uniq) then begin
                                Seek(X + O);
                                if (Not Testing) then begin
                                    CheckMemory(Sizeof(TDiskFile));
									{$IFOPT D+} Debug('+OPEN', Output + TempDir + TempName + ' at ' + IntStr(Offset)); {$ENDIF}
                                    {$IFDEF FileTemp}
	                                    FileTemp := New(PDiskFile, Create(nil));
    	                                FileTemp^.Assign(Output + TempDir + TempName);
										if (Offset = 0) then
											FileTemp^.Rewrite
										else
											FileTemp^.Reset;
										CheckError(FileTemp, True);
										FileTemp^.Seek(Offset);
										CheckError(FileTemp, True);
    	                            {$ELSE}
    	                            	IOE := System.IOResult; { just make sure it is cleared }
										FileMode := 2;
    	                            	System.Assign(F, Output + TempDir + TempName);
    	                            	IOE := System.IOResult; { nothing has really happened yet }
										ROE := 5;
										repeat
											if (Offset = 0) then
												System.Rewrite(F, 1)
											else
												System.Reset(F, 1);
											Dec(ROE);
											if ROE > 0 then
												IOE := System.IOResult
											else
												IOE := GetIOResult(TempName, True);
											if IOE <> 0 then begin
												{$IFOPT D+} Debug('RETRY', 'FILE ERROR ' + INtStr(IOE) ); {$ENDIF}
												SetFAttr(F, 0);
												if ROE < 4 then Delay(250);
											end;
										until IOE = 0;
										System.Seek(F, Offset);
    	                            	IOE := GetIOResult(TempName, true);
    	                            {$ENDIF}
                                end;
                                while (S > 0) and (FilePos < FileSize) do begin
                                    if S > BufferSize then
                                        ToRead := BufferSize
                                    else
                                        ToRead := S;
                                    ReadRecord(Buf^, ToRead);
									{$IFOPT D+} Debug('READ', IntStr(ToRead) + ' of ' + IntStr(S) + ' bytes'); {$ENDIF}
                                    CheckError(@Self, True);
                                    LastSig := Signature(Buf^, ToRead, LastSig);
                                    if (not Testing) then begin
										{$IFOPT D+} Debug('WRITE', IntStr(ToRead) + ' bytes'); {$ENDIF}
										{$IFDEF FileTemp}
	                                        FileTemp^.WriteRecord(Buf^, ToRead);
    	                                    CheckError(FileTemp, True);
    	                                {$ELSE}
    	                                	System.BlockWrite(F, Buf^, ToRead);
											IOE := GetIOResult(TempName, true);
										{$ENDIF}

                                    end;
                                    Dec(S, ToRead);
                                end;
                                {$IFDEF FileTemp}
                                if Assigned(FileTemp) then begin
                                {$ELSE}
                                if (Not Testing) then begin
                                {$ENDIF}
									{$IFOPT D+} Debug('CLOSE', ''); {$ENDIF}
									{$IFOPT D+} Debug('-', ''); {$ENDIF}
									{$IFDEF FileTemp}
										Dispose(FileTemp, Destroy);
										FileTemp := nil;
									{$ELSE}
										System.Close(F);
										IOE := GetIOResult(TempName, true);
									{$ENDIF}
                                end;
                                if Offset = 0 then begin
                                    Inc(Summary.FileTotal);
                                    Inc(Summary.ByteTotal, Size);
                                end;
{$IFOPT D+} Debug('sig', HexLong(LastSig)); {$ENDIF}
                            end;
                        end;
                        with TSAF_File(P^)do begin
                            LastFile := Uniq;
                            LastStamp := Stamp;
                            LastAttr := Attrib;
                            LastComp := CompMethod;
                        end;
                        LastFileName := TempDir + TempName;
                    end;
{$IFOPT D+} Debug('-', ''); {$ENDIF}
                end;
                scVerify : begin
{$IFOPT D+} Debug('+verify', ''); {$ENDIF}
                    UserAccept(NeedConf);
                    TempLang := StringDelim;
                    if ThisCat then begin
                        if (Chunk.Block.Size <> Sizeof(TSAF_Verify)) then
                            MakeError(@Self, erInvalid_Data, True);
                        ReadRecord(Chunk.Data, Chunk.Block.Size - Sizeof(Chunk.Block) );
                        if (TSAF_Verify(P^).Uniq <> LastFile) then
                            MakeError(@Self, erInvalid_Data, True);
{$IFOPT D+} Debug('', '[' + IntStr(TSAF_Verify(P^).Uniq) + '] ' + HexLong(TSAF_Verify(P^).Signature)); {$ENDIF}
                        if (TSAF_Verify(P^).Uniq <> LastSkip) then
                            with TSAF_Verify(P^) do begin
                                MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix + 'SIG',
                                    IntStr(Block.ID) + FormatDelim + SizeStr(Block.Size) + FormatDelim +
                                    HexLong(Signature) + FormatDelim + IntStr(Uniq) ));
                        end;
                        if (Not Report) and (TSAF_Verify(P^).Uniq <> LastSkip)
                        and (TSAF_Verify(P^).Signature <> LastSig) then begin
                            if Testing then
                                ShowError('', IntStr(erData_Error) + FormatDelim + LastFileName, erData_Error, False )
                            else if AskUser('STOP', LastFileName) then
                                ShowError('', IntStr(erData_Error) + FormatDelim + LastFileName, erData_Error, True );
                        end else if (LastStamp <> -1) and (not (Testing or Report)) then begin
                            if (LastComp <> cpNone) and (LastSkip <> TSAF_File(P^).Uniq) then
                        		DecompressFile(Output + LastFileName, LastComp);

							(* FillChar(F, Sizeof(F), 0);
                            System.Assign(F, Output + LastFileName);
							SetFTime(F, LastStamp);

							ShowError('', IntStr(DosError) + FormatDelim + LastFileName, DosError, False );
							{$IFOPT D+} Debug('STAMP', Output + LastFileName + ' ' + StampStr(LastStamp) + ' = ' + IntStr(DosError)); {$ENDIF}

                            if (CreateMode = 0) or (LastAttr <> CreateMode) then begin
								{$IFOPT D+} Debug('ATTRIB', Output + LastFileName + ' ' + HEXWord(LastAttr)); {$ENDIF}
                                SetFAttr(F, LastAttr);
                                ShowError('', IntStr(DosError) + FormatDelim + LastFileName, DosError, False );
                            end;
                            *)
                            FileStamp(Output + LastFileName, LastStamp, LastAttr);
							{$IFOPT D+} Debug('STAMP', Output + LastFileName + ' ' + StampStr(LastStamp) + ' = ' + IntStr(DosError)); {$ENDIF}
                        end;
                    end;
                    LastFileName := '';
{$IFOPT D+} Debug('-', ''); {$ENDIF}
{$IFOPT D+} Debug('-', ''); {$ENDIF}
                end;
                scMessage : begin
{$IFOPT D+} Debug('+message', ''); {$ENDIF}
                    if ThisCat then begin
                        if Chunk.Block.Size <= Sizeof(TSAF_Message) - Sizeof(TFileName) + 1 then
                            MakeError(@Self, erInvalid_Data, True);
                        BlockRead(Chunk.Data, Sizeof(TSAF_Message) - Sizeof(Chunk.Block), BytesRead);
                        if LastText <> TSAF_Message(P^).Uniq then
                            UserAccept(NeedConf);
                        TempName := Ucase(Trim(PascalStr(TSAF_Message(P^).Language)));
                        O := Sizeof(TSAF_Message) - Sizeof(TFileName) + Length(TempName) + 1;
                        S := Chunk.Block.Size - O;
                        if Report then with TSAF_Message(P^) do begin
                            MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix +
                                WhichStr(Uniq = LastText, WhichStr(Level <> 0, 'CONF', 'TEXT'), 'MORE'),
                                IntStr(Block.ID) + FormatDelim + SizeStr(Block.Size) + FormatDelim +
                                TempName + FormatDelim + IntStr(Uniq) + FormatDelim +
                                IntStr(Level) + ChrStr(FormatDelim, 4) +
                                SizeStr(S) + ChrStr(FormatDelim, 2)));
                        end;
                        LastText := TSAF_Message(P^).Uniq;
                        if (not Report) and (
                        ((TempName = '') or (TempName = Language)) or
                        ((TempName = '*') and (Pos(StringDelim + Language + StringDelim, TempLang) < 1))
                        ) then begin
                            NeedConf := NeedConf or ((TSAF_Message(P^).Level < mcCritical) and (not Report));
                            if NeedConf then
                                ThemeColor(clAcceptText)
                            else
                                ThemeColor(clEmbedText);
                            Seek(X + O);
                            O := S;
                            while (O > 0) {$IFOPT D+} and (DebugSize > FilePos) {$ENDIF} do begin
                                S := O;
                                if S > BufferSize then
                                    S := BufferSize;
                                BlockRead(Buf^, S, BytesRead);
                                Dec(O, BytesRead);
                                for S := 0 to BytesRead - 1 do
                                    EmbeddedText(TSAF_Message(P^).Level, PCharArray(Buf)^[S]);

                            end;
                            ThemeColor(clNormal);
                        end;
                        if TempName = '*' then
                            TempLang := StringDelim
                        else if (Pos(StringDelim + Language + StringDelim, TempLang) < 1) then
                            TempLang := TempLang + TempName + StringDelim;
                    end;
{$IFOPT D+} Debug('-', ''); {$ENDIF}
                end;
            else
{$IFOPT D+} Debug('+other', ''); {$ENDIF}
                UserAccept(NeedConf);
                TempLang := StringDelim;
                with TSAF_AnyChunk(P^) do
                    MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix + 'OTHER',
                        IntStr(Block.ID) + FormatDelim + SizeStr(Block.Size) ));
{$IFOPT D+} Debug('-', ''); {$ENDIF}
            end;
            Inc(X, Chunk.Block.Size);
            Seek(X);
{$IFOPT D+} Debug('-', ''); {$ENDIF}
        end;
        if Info.PartIndex + 1 < Info.PartTotal then begin
            Close;
            Inc(Info.PartIndex);
            Assign(ArchiveName);
            Reset;
            MaybeTextLn(mcGeneral, ParseMessage(MsgPrefix + 'SLICE', Name));
            ReadHeader(not Report);
        end else Break;
    until False;
{$IFOPT D+} Debug('-', ''); {$ENDIF}
  GetMem(Buf, BufferSize);
{  Buf := nil; }
  ArchiveSummary;
end;
